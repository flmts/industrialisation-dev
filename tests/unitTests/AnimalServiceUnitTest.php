<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use PHPUnit\Framework\TestCase;

require_once __DIR__ . '/../../src/invalidInputException.php';
use src\InvalidInputException;

require_once __DIR__ . '/../../src/AnimalService.php';

/**
 * * @covers invalidInputException
 * @covers \AnimalService
 *
 * @internal
 */
final class AnimalServiceUnitTest extends TestCase {
    private $animalService;

    public function __construct(string $name = null, array $data = [], $dataName = '') {
        parent::__construct($name, $data, $dataName);
        $this->animalService = new AnimalService();
    }


    public function testCreationAnimalWithoutAnyText() {
        $this->expectException(invalidInputException::class);
        $this->animalService->createAnimal('', '');
    }

    public function testCreationAnimalWithoutName() {
        $this->expectException(invalidInputException::class);
        $this->animalService->createAnimal('', '123');
    }

    public function testCreationAnimalWithoutNumber() {
        $this->expectException(invalidInputException::class);
        $this->animalService->createAnimal('Name',152);
    }

    public function testSearchAnimalWithNumber() {
        $this->expectException(invalidInputException::class);
        $this->animalService->searchAnimal(123);
    }

    public function testModifyAnimalWithInvalidId() {
        $this->expectException(invalidInputException::class);
        $this->animalService->updateAnimal('', 'Name', '123');
    }

    public function testDeleteAnimalWithTextAsId() {
        $this->expectException(invalidInputException::class);
        $this->animalService->deleteAnimal('text');
    }

    //Tests sur getAnimal
    /*public function testGetAnimalWithValidIdReturnsDetails(): void {
        // ID d'un animal existant dans la base de données
        $existingId = '001';

        $animalDetails = $this->animalService->getAnimal($existingId);

        $this->assertIsArray($animalDetails);
        $this->assertArrayHasKey('id', $animalDetails);
        // Ajoutez d'autres assertions pour vérifier les détails de l'animal récupéré
    }*/

    public function testGetAnimalWithEmptyIdThrowsException(): void {
        $this->expectException(invalidInputException::class);
        $this->animalService->getAnimal('');
    }

    public function testGetAnimalWithNonNumericIdThrowsException(): void {
        $this->expectException(invalidInputException::class);
        $this->animalService->getAnimal('invalidId');
    }

    public function testGetAnimalWithNegativeIdThrowsException(): void {
        $this->expectException(invalidInputException::class);
        $this->animalService->getAnimal(-1);
    }

    public function testGetAnimalWithNonExistingIdThrowsException(): void {
        // ID inexistant dans la base de données
        $nonExistingId = 9999;

        $this->expectException(invalidInputException::class);
        $this->animalService->getAnimal($nonExistingId);
    }

    //test sur searchAnimal
    /*
    public function testSearchAnimalWithValidTermReturnsResults(): void {
        // Terme correspondant à des animaux existants
        $existingTerm = 'koala';

        $animalResults = $this->animalService->searchAnimal($existingTerm);

        $this->assertIsArray($animalResults);
    }
    */

    public function testSearchAnimalWithNonExistingTermReturnsEmptyArray(): void {
        // Terme ne correspondant à aucun animal existant
        $nonExistingTerm = 'koala';

        $this->expectException(invalidInputException::class);
        $this->animalService->searchAnimal($nonExistingTerm);

    }

    public function testSearchAnimalWithEmptyTermThrowsException(): void {
        $this->expectException(invalidInputException::class);
        $this->animalService->searchAnimal('');
    }

    public function testSearchAnimalWithNonStringTermThrowsException(): void {
        $this->expectException(invalidInputException::class);
        $this->animalService->searchAnimal('oui'); // Terme non autorisé (non-string)
    }


    //Tests sur getAllAnimals
    public function testGetAllAnimalsReturnsNonEmptyArray(): void
    {
        $animalService = new AnimalService();
        $animalService->createAnimal('ui', '001');
        $animalService->createAnimal('lapin', '002');

        $animals = $animalService->getAllAnimals();

        $this->assertNotEmpty($animals);
        $this->assertIsArray($animals);
    }

    public function testGetAllAnimalsReturnsEmptyArrayWhenNoAnimals(): void
    {
        $animalService = new AnimalService();
        $animalService->deleteAllAnimal();

        $animals = $animalService->getAllAnimals();

        $this->assertEmpty($animals);
        $this->assertIsArray($animals);

        //Re création de deux animaux pour le bien des autres tests
        $animalService->createAnimal('koala', '002');
        $animalService->createAnimal('lapin', '002');
    }


    //Tests sur createAnimal
    public function testCreateAnimalWithValidData(): void
    {
        $animalService = new AnimalService();

        $isAnimalCreated = $animalService->createAnimal('3', '123456');

        $this->assertTrue($isAnimalCreated);
    }

    public function testCreateAnimalWithInvalidDataThrowsException(): void
    {
        $animalService = new AnimalService();

        $this->expectException(invalidInputException::class);
        $animalService->createAnimal('', '');
    }
    public function testCreateAnimalWithEmptyNameThrowsException(): void {
        $this->expectException(invalidInputException::class);
        $this->animalService->createAnimal('', '1234');
    }

    public function testCreateAnimalWithEmptyIdentificationNumberThrowsException(): void {
        $this->expectException(invalidInputException::class);
        $this->animalService->createAnimal('Name', '');
    }

    //Tests pour updateAnimal
    public function testUpdateAnimalSuccessful(): void {
        // Insertion initiale d'un animal
        $this->animalService->createAnimal('Chien', '123');

        // Récupération de l'animal inséré
        $animal = $this->animalService->searchAnimal('Chien');

        // Mise à jour de l'animal
        $result = $this->animalService->updateAnimal($animal['id'], 'l', '456');

        $this->assertTrue($result);
    }

    public function testUpdateAnimalWithNonExistingIdThrowsException(): void {
        $this->expectException(invalidInputException::class);
        $this->animalService->updateAnimal(9999, 'Chien', '123');
    }

    public function testUpdateAnimalWithInvalidIdThrowsException(): void {
        $this->expectException(invalidInputException::class);
        $this->animalService->updateAnimal('invalid_id', 'Chien', '123');
    }

    public function testUpdateAnimalWithInvalidDataThrowsException(): void {
        $this->expectException(invalidInputException::class);
        $this->animalService->updateAnimal(1, '', '123');
    }


}
