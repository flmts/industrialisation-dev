<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use PHPUnit\Framework\TestCase;

require __DIR__ . '/../../src/AnimalService.php';

/**
 * * @covers invalidInputException
 * @covers \AnimalService
 *
 * @internal
 */
final class AnimalServiceIntegrationTest extends TestCase
{
    private $animalService;

    public function __construct(string $name = null, array $data = [], $dataName = '') {
        parent::__construct($name, $data, $dataName);
        $this->animalService = new AnimalService();
    }

    // test de suppression de toute les données, nécessaire pour nettoyer la bdd de tests à la fin
    public function testDeleteAll()
    {
        $result = $this->animalService->deleteAllAnimal();
        $this->assertTrue($result !== false);
    }

    public function testCreation()
    {
        $nom = 'AnimalTest';
        $numeroIdentification = '2';

        $result = $this->animalService->createAnimal($nom, $numeroIdentification);

        $this->assertTrue($result);
    }

    public function testSearch()
    {
        $searchTerm = 'AnimalTest';

        $animals = $this->animalService->searchAnimal($searchTerm);

        $this->assertIsArray($animals);
    }

    public function testModify()
    {
        $animals = $this->animalService->getAllAnimals();

        if (!empty($animals)) {
            $animal = $animals[0];
            $updatedName = 'UpdatedAnimal';

            $result = $this->animalService->updateAnimal($animal['id'], $updatedName, $animal['numeroIdentifcation']);

            $this->assertTrue($result);
        } else {
            $this->markTestSkipped('No animals found for modification test.');
        }
    }

    public function testDelete()
    {
        $animals = $this->animalService->getAllAnimals();

        if (!empty($animals)) {
            $animal = $animals[0];

            $result = $this->animalService->deleteAnimal($animal['id']);

            $this->assertTrue($result);
        } else {
            $this->markTestSkipped('No animals found for deletion test.');
        }
    }

}
